provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "kubernetes-admin@cluster.local"
}


#create ns 
resource "kubernetes_namespace" "ns-test" {
  metadata {
    name = "ns-test"
  }
}

#create deployment
resource "kubernetes_deployment" "deployment-test" {
  metadata {
    name = "deployment-test"
    namespace = kubernetes_namespace.ns-test.metadata.0.name
    labels = {
      test = "MyExampleApp"
    }
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        test = "MyExampleApp"
      }
    }

    template {
      metadata {
        labels = {
          test = "MyExampleApp"
        }
      }

      spec {
        container {
          image = "nginx:latest"
          name  = "nginx-container-test"

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }

          liveness_probe {
            http_get {
              path = "/"
              port = 80

              http_header {
                name  = "X-Custom-Header"
                value = "Awesome"
              }
            }

            initial_delay_seconds = 3
            period_seconds        = 3
          }
        }
      }
    }
  }
}

#create service 
resource "kubernetes_service" "service-test" {
  metadata {
    name = "service-test"
    namespace = kubernetes_namespace.ns-test.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.deployment-test.metadata.0.labels.test
    }
    session_affinity = "ClientIP"
    port {
      port        = 80
      target_port = 80
    }

    type = "LoadBalancer"
  }
}

#create ingress
resource "kubernetes_ingress" "ingress-test" {
  metadata {
    name = "ingress-test"
    namespace = kubernetes_namespace.ns-test.metadata.0.name
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
      "app.kubernetes.io/name" = "ingress-nginx"
    }
  }
  spec {
    rule {
      host =  "test.example.com"
      http {
        path {
          path = "/*"
          backend {
            service_name = kubernetes_service.service-test.metadata.0.name
            service_port = 80
          }
        }
      }
    }
  }
}



